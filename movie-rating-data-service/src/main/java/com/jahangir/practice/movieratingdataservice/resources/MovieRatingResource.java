package com.jahangir.practice.movieratingdataservice.resources;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jahangir.practice.movieratingdataservice.models.Rating;
import com.jahangir.practice.movieratingdataservice.models.UserRating;

@RestController
@RequestMapping("/movierating")
public class MovieRatingResource {

	@GetMapping(path = "/{movieId}")
	public Rating getRating(@PathVariable String movieId) {
		return new Rating(movieId, 3);
	}
	
	@GetMapping(path = "users/{userId}")
	public UserRating getUserRating(@PathVariable String userId) {
		List<Rating> ratings = Arrays.asList(
				new Rating("500", 4),
				new Rating("550",3));
		UserRating userRatinh = new UserRating();
		userRatinh.setRatings(ratings);
		return userRatinh;
	}
}
