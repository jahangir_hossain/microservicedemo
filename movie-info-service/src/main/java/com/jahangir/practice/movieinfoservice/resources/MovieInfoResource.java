package com.jahangir.practice.movieinfoservice.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.jahangir.practice.movieinfoservice.models.Movie;
import com.jahangir.practice.movieinfoservice.models.MovieSummary;

@RestController
@RequestMapping("/movies")
public class MovieInfoResource {

	@Value("${api.key}")
	private String apiKey;

	@Value("${api.baseUri}")
	private String apiBaseUri;

	@Autowired
	WebClient.Builder webClientBuilder;

	@GetMapping(path = "/{movieId}")
	public Movie getMovieById(@PathVariable("movieId") String movieId) {
		
		System.out.println("Basie URL: "+ apiBaseUri + "\nApi key: " +apiKey);

		MovieSummary movieSummary = webClientBuilder
				.build()
				.get()
				.uri(apiBaseUri+movieId+apiKey)
				.retrieve()
				.bodyToMono(MovieSummary.class)
				.block();
		return new Movie(movieId, movieSummary.getTitle(), movieSummary.getOverview());
	}
}
